﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController Instance;
    public CameraController CameraController => cameraController;
    public GameConfig GameConfig => gameConfig;

    [SerializeField]
    private CameraController cameraController;

    [SerializeField]
    private GameObject figureRoot;

    [SerializeField]
    private GameConfig gameConfig;

    [HideInInspector, SerializeField]
    private List<GameObject> figures = new List<GameObject>();

    [HideInInspector, SerializeField] private ColorDataBase colorsData = new ColorDataBase();

    private void Awake()
    {
        if (Instance != null)
            Destroy(gameObject);
        Instance = this;
    }

    private void Start()
    {
        Load();
        Save();
        
        var screen = GUIController.Instance.FoundScreen<ScreenChooseObject>();
        foreach (var figure in gameConfig.SceneObjects)
        {
            var index = gameConfig.SceneObjects.FindIndex(x => x == figure);

            GameObject figureGO = Instantiate(figure, figureRoot.transform);
            Renderer rend = figureGO.GetComponent<Renderer>();
            rend.material = new Material(Shader.Find("Standard"));
            figures.Add(figureGO);
            screen.CreateFigureButton(figureGO);
            if(colorsData != null)
            {
                if (gameConfig.SceneObjects.Count == colorsData.colorDataList.Count)
                {
                    var rgba = colorsData.colorDataList[index].rgba;
                    figureGO.GetComponent<Renderer>().sharedMaterial.color = new Color(rgba[0], rgba[1], rgba[2], rgba[3]);
                }
            }
        }
    }

    private void OnApplicationQuit()
    {
        if(colorsData.colorDataList.Count != 0)
            colorsData.colorDataList.Clear();

        foreach(var figure in figures)
        {
            var color = figure.GetComponent<Renderer>().sharedMaterial.color;
            SaveData saveData = new SaveData();

            saveData.rgba[0] = color.r;
            saveData.rgba[1] = color.g;
            saveData.rgba[2] = color.b;
            saveData.rgba[3] = color.a;

            if(colorsData == null)
            {
                colorsData = new ColorDataBase();
            }
            colorsData.colorDataList.Add(saveData);
        }
        Save();
    }

    void Load()
    {
        if (File.Exists(FilePath))
        {
            colorsData = JsonUtility.FromJson<ColorDataBase>(File.ReadAllText(FilePath));
        }
        else Debug.LogError("Data file wasn't found");
    }

    void Save()
    {
        File.WriteAllText(FilePath, JsonUtility.ToJson(colorsData, true));
    }

    private string FilePath
    {
        get { return Path.Combine(Application.persistentDataPath, "ColorData.json"); }
    }
}

[Serializable]
public class ColorDataBase
{
    public List<SaveData> colorDataList = new List<SaveData>();
}

[Serializable]
public class SaveData
{
    public float[] rgba = new float[4];
}
