﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class GridObjects : MonoBehaviour
{
    public float spacing;
    public int constraintCount = 1;


    void OnValidate()
    {
        constraintCount = Mathf.Max(constraintCount, 1);
        spacing = Mathf.Max(0, spacing);
    }

    void Update()
    {
        int i = 0;
        float minX = int.MaxValue, maxX = int.MinValue;
        foreach(Transform item in transform)
        {
            int x;
            x = i / constraintCount;

            item.localPosition = new Vector3(x * spacing, 0, 0);
            minX = Mathf.Min(minX, x * spacing);
            maxX = Mathf.Max(maxX, x * spacing);

            i++;
        }

        Vector3 center = new Vector3((minX + maxX) / 2f, 0, 0);

        foreach (Transform item in transform)
        {
            item.localPosition -= center;
        }
    }
}
