﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform choosenTarget;

    [SerializeField]
    private float speed = 0.2f;
    [SerializeField]
    private Vector3 offset;

    private Vector3 defaultPosition;

    private void Start()
    {
        foreach (var item in GameController.Instance.GameConfig.SceneObjects)
        {
            defaultPosition.z += offset.z;
        }
    }

    private void FixedUpdate()
    {
        if (choosenTarget == null)
            transform.position = Vector3.Lerp(transform.position, defaultPosition, speed);
        else
        {
            Vector3 pos = choosenTarget.position + offset;
            Vector3 smoothPos = Vector3.Lerp(transform.position, pos, speed);
            transform.position = smoothPos;
        }
    }
}
