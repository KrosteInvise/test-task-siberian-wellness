﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ScreenChooseColor : GUIScreen
{
    [SerializeField]
    private Button backButton;

    [SerializeField]
    private Transform colorPanelRoot;

    [SerializeField]
    private List<ColorCell> colorPanelList = new List<ColorCell>();

    private void Start()
    {
        var colorPanel = GameController.Instance.GameConfig.Colors;
        var cell = GameController.Instance.GameConfig.cell;
        int i = 0;

        foreach (var panel in colorPanel)
        {
            var panelGO = Instantiate(cell, colorPanelRoot);
            panelGO.panelColor = colorPanel[i];
            colorPanelList.Add(panelGO);
            i++;
        }
        backButton.onClick.AddListener(BackToMainScreen);
        
        colorPanelList.ForEach(x => x.Init());
        colorPanelList.ForEach(x => x.PaintButton.onClick.AddListener(CheckAfterClick));
        CheckAfterClick();
    }

    private void OnEnable()
    {
        CheckAfterClick();
    }

    void BackToMainScreen()
    {
        GUIController.Instance.HideScreen<ScreenChooseColor>();
        GUIController.Instance.ShowScreen<ScreenChooseObject>();
        GameController.Instance.CameraController.choosenTarget = null;
    }

    public void CheckAfterClick()
    {
        foreach (var panel in colorPanelList)
        {
            if (GameController.Instance.CameraController.choosenTarget != null)
            {
                var figureRenderer = GameController.Instance.CameraController.choosenTarget.GetComponent<Renderer>();
                if (figureRenderer.sharedMaterial.color == panel.PanelColor)
                    panel.ChooseBlock();
                else
                    panel.UnchooseBlock();
            }
        }
    }
}
