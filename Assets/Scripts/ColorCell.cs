﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class ColorCell : MonoBehaviour
{
    [Header("Choosen options")]
    [SerializeField]
    private Image choosenImage;
    [SerializeField]
    private Text choosenText;
    [SerializeField]
    private Image buttonImage;

    [Header("Figures Settings")]
    [SerializeField]
    public Color panelColor;
    [SerializeField]
    private Button paintButton;

    private bool choosen;

    public Color PanelColor => panelColor;
    public Button PaintButton => paintButton;


    public void Init()
    {
        buttonImage.color = panelColor;
        paintButton.onClick.AddListener(PaintFigure);
    }

    void PaintFigure()
    {
        if (!choosen)
        {
            var figureRenderer = GameController.Instance.CameraController.choosenTarget.GetComponent<Renderer>();
            figureRenderer.sharedMaterial.SetColor("_Color", panelColor);
            ChooseBlock();
        }
    }

    public void ChooseBlock()
    {
        choosen = true;
        choosenText.gameObject.SetActive(true);
        paintButton.interactable = false;
    }

    public void UnchooseBlock()
    {
        choosen = false;
        choosenText.gameObject.SetActive(false);
        paintButton.interactable = true;
    }
}
