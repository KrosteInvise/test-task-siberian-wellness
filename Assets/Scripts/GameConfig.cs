﻿using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameConfig", menuName = "GameConfig")]
public class GameConfig : ScriptableObject
{
    [Header("Scene Objects")]
    public List<GameObject> SceneObjects = new List<GameObject>();

    [Header("Color Settings")]
    public List<Color> Colors = new List<Color>();

    [Header("Cell Prefab")]
    public ColorCell cell;
    
}
