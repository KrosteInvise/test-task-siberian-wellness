﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIController : MonoBehaviour
{
    public static GUIController Instance;

    [SerializeField]
    private GameObject screenRoot;
    [SerializeField]
    private List<GameObject> ScreenPrefabs = new List<GameObject>();
    private List<GUIScreen> screens = new List<GUIScreen>();

    private void Awake()
    {
        if (Instance != null)
            Destroy(gameObject);
        Instance = this;
    }

    private void Start()
    {
        foreach (var screen in ScreenPrefabs)
        {
            GameObject screenGO = Instantiate(screen, screenRoot.transform);
            screens.Add(screenGO.GetComponent<GUIScreen>());
        }
        foreach (var screen in screens)
        {
            if (!screen.main)
                screen.gameObject.SetActive(false);
            else
                screen.gameObject.SetActive(true);
        }
    }

    public void ShowScreen<T>() where T : GUIScreen
    {
        GUIScreen foundScreen = FoundScreen<T>();
        if (foundScreen != null)
        {
            foundScreen.Show();
        }
        else Debug.LogWarning("Screen not found!");
    }

    public T FoundScreen<T>() where T : GUIScreen
    {
        foreach (var curScreen in screens)
        {
            if (curScreen.GetType() == typeof(T))
                return curScreen as T;
        }
        return null;
    }
    
    public void HideScreen<T>() where T : GUIScreen
    {
        GUIScreen foundScreen = FoundScreen<T>();
        if (foundScreen != null)
        {
            foundScreen.Hide();
        }
        else Debug.LogWarning("Screen not found!");
    }
}
