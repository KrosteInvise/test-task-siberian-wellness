﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenChooseObject : GUIScreen
{
    [SerializeField]
    private GameObject buttonsRoot;

    [SerializeField]
    private GameObject figureButtonPrefab;

    public void CreateFigureButton(GameObject figure)
    {
        GameObject buttonGO = Instantiate(figureButtonPrefab, buttonsRoot.transform);
        buttonGO.GetComponentInChildren<Text>().text = figure.name.Replace("(Clone)", "");
        buttonGO.GetComponent<Button>().onClick.AddListener(() =>
        {
            GameController.Instance.CameraController.choosenTarget = figure.transform;
            GUIController.Instance.ShowScreen<ScreenChooseColor>();
            GUIController.Instance.HideScreen<ScreenChooseObject>();
        });
    }
}
