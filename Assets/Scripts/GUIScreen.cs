﻿using UnityEngine;
using System.Collections.Generic;

public class GUIScreen : MonoBehaviour
{
    public static GUIScreen Instance;

    public bool main = false;


    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

}
